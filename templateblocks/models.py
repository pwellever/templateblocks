from django.core.cache import cache
from django.db import models
from django.utils.html import linebreaks
import markdown

from templateblocks.settings import CACHE_PREFIX

def do_markdown(text):
    return markdown.markdown(text,
                             output_format='html5',
                             extensions=['extra', 'nl2br', 'smarty', 'toc'])

class TemplateBlock(models.Model):
    """
    A text snippet for use in templates
    """
    RAW = 1
    BR = 2
    MD = 3
    FORMAT_CHOICES = (
        (RAW, 'None'),
        (BR, 'Convert linebreaks'),
        (MD, 'Markdown'),
    )
    slug = models.CharField(max_length=255, unique=True,
                            help_text="A unique name used for reference in \
                                       the templates")
    format = models.IntegerField('formatting', choices=FORMAT_CHOICES, default=BR)
    content = models.TextField(blank=True)
    content_html = models.TextField(editable=False, blank=True)
    note = models.CharField(blank=True, max_length=255,
                            help_text="Optional administrative note")

    # Helper attributes used if content should be evaluated in order to
    # represent the original content.
    raw_content = None
    raw_header = None

    def __unicode__(self):
        return u'%s' % (self.slug,)

    def save(self, *args, **kwargs):
        if self.format == self.MD:
            self.content_html = do_markdown(self.content)
        elif self.format == self.BR:
            self.content_html = linebreaks(self.content)
        else:
            self.content_html = self.content
        super(TemplateBlock, self).save(*args, **kwargs)
        # Now also invalidate the cache used in the templatetag
        cache.delete('%s%s' % (CACHE_PREFIX, self.slug, ))

    def delete(self, *args, **kwargs):
        cache_key = '%s%s' % (CACHE_PREFIX, self.slug,)
        super(TemplateBlock, self).delete(*args, **kwargs)
        cache.delete(cache_key)
