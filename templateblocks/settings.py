from django.conf import settings

CACHE_PREFIX = getattr(settings, 'TEMPLATEBLOCKS_CACHE_PREFIX', 'templateblocks_')
AUTOCREATE_STATIC_BLOCKS = getattr(settings,
                                   'TEMPLATEBLOCKS_AUTOCREATE_STATIC_BLOCKS',
                                   True)
