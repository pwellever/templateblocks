from django import template
from django.db import models
from templateblocks import settings

register = template.Library()
TemplateBlock = models.get_model('templateblocks', 'templateblock')

class BlockWrapper(object):
    def prepare(self, parser, token):
        tokens = token.split_contents()
        self.is_variable = False
        self.slug = None
        tag_name, self.slug, args = tokens[0], tokens[1], tokens[2:]
        
        # Check to see if the slug is properly double/single quoted
        if not (self.slug[0] == self.slug[-1] and self.slug[0] in ('"', "'")):
            self.is_variable = True
        else:
            self.slug = self.slug[1:-1]

    def __call__(self, parser, token):
        self.prepare(parser, token)
        return BlockNode(self.slug, self.is_variable)


do_get_templateblock = BlockWrapper()


class BlockNode(template.Node):
    def __init__(self, slug, is_variable):
        self.slug = slug
        self.is_variable = is_variable

    def render(self, context):
        if self.is_variable:
            real_slug = template.Variable(self.slug).resolve(context)
        else:
            real_slug = self.slug

        try:
            # if slug is hard-coded in template then auto-create block 
            # according to the TEMPLATEBLOCKS_AUTOCREATE_STATIC_BLOCKS setting
            if self.is_variable or not settings.AUTOCREATE_STATIC_BLOCKS:
                templateblock = TemplateBlock.objects.get(slug=real_slug)
            else:
                templateblock, _ = TemplateBlock.objects.get_or_create(slug=real_slug)

            output = self._evaluate(templateblock.content_html, context)
            return output

        except TemplateBlock.DoesNotExist:
            return ''

    def _evaluate(self, content, context):
        return template.Template(content).render(context)

register.tag('templateblock', do_get_templateblock)
