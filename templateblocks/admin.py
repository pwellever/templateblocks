import re
from django.contrib import admin
from django.core import serializers
from django.http import HttpResponse
from django.template.defaultfilters import truncatechars
from django.utils.html import strip_tags
from templateblocks.models import TemplateBlock

def export_json(modeladmin, request, queryset):
    data = serializers.serialize('json',
                                 list(queryset),
                                 indent=4,
                                 use_natural_keys=True)
    data = re.sub('"pk": [0-9]{1,5}', '"pk": null', data)
    return HttpResponse(data, mimetype="application/json")

class HasContentFilter(admin.SimpleListFilter):
    title = 'content'
    parameter_name = 'empty'
    
    def lookups(self, request, model_admin):
        return (
            ('y', 'Empty'),
            ('n', 'Not Empty'),
        )
    
    def queryset(self, request, queryset):
        if self.value() == 'y':
            return queryset.filter(content='')
        if self.value() == 'n':
            return queryset.exclude(content='')


class TemplateBlockAdmin(admin.ModelAdmin):
    ordering = ['slug', ]
    list_display = ('slug', 'excerpt', 'note', 'has_content')
    list_filter = (HasContentFilter,)
    search_fields = ('slug', 'note', 'content')
    radio_fields = {"format": admin.VERTICAL}
    actions = (export_json,)

    def excerpt(self, obj):
        return truncatechars(strip_tags(obj.content_html), 40)

    def has_content(self, obj):
        return bool(obj.content)
    has_content.boolean = True

admin.site.register(TemplateBlock, TemplateBlockAdmin)
