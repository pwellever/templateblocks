from distutils.core import setup

setup(
    name='templateblocks',
    packages=['templateblocks', 'templateblocks.templatetags'],
)